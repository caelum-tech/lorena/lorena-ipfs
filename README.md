# Lorena - IPFS

An IPFS interface in Javascript for SSI applications.

## Requirements

`IPFS` must be installed:

```bash
# Example with `snapd`
sudo snap install ipfs
ipfs init
ipfs run daemon
npm i
```

## Getting started

First install `lorena-ipfs` as a dependency.

```bash
npm install @caelum-tech/lorena-ipfs
```

## Contributing

Please, contribute to any `lorena` public project! The more the better! Feel free to to open an issue and/or contacting directly with the owner for any request or suggestion.

## Acknowledgment

This library is created to satisfy SSI-related w3's specifications.

## Code of conduct

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License

This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in [LICENSE-APACHE](LICENSE-APACHE.txt) and [LICENSE-MIT](LICENSE-MIT.txt) respectively.

## Related repositories

* [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground)
* [Demo website: Hoverfloat](https://gitlab.com/caelum-tech/Lorena/lorena-demo-hoverfloat)
* [Lorena Substrate](https://gitlab.com/caelum-tech/Lorena/lorena-substrate)
* [Lorena Matrix Client](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client)
* [Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon)
* [Lorena Matrix Helpers](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers)
* [caelum-diddoc](https://gitlab.com/caelum-tech/caelum-diddoc)
