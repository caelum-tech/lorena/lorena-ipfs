const chai = require('chai')
const IPFS = require('../src/index.js')
const uuidv4 = require('uuid/v4')
const diddoc = require('@caelum-tech/caelum-diddoc-nodejs')

// Configure chai
chai.should()
const expect = chai.expect

let ipfs, file
const did = 'did:lor:cat:lab:org:' + uuidv4()

describe('Interplanetary File System', function () {
  it('should create a new IPFS instance', () => {
    ipfs = new IPFS()
  })
  /* eslint-disable */
  it('should detect the IPFS daemon running', async () => {
    expect(await ipfs.connected()).to.be.true
  })
  /* eslint-enable */
  it('should put a new file', async () => {
    const didDocument = new diddoc.DidDoc('https://www.w3.org/ns/did/v1', did)
    file = await ipfs.add(didDocument.toJSON())
  })

  it('should get that file', async () => {
    const result = await ipfs.get(file[0].path)
    result.id.should.eq(did)
  })
})
